import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Doughnut} from 'react-chartjs-2';

class M_Doughnut extends Component {

    data = () => {
        return {
            labels: [
                'Red',
                'Green',
                'Yellow'
            ],
            datasets: [{
                data: [300, 50, 100],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56'
                ]
            }]
        }
    };

    render() {
        return (
            <div>
                <h2>Doughnut Example</h2>
                <Doughnut data={this.data}/>
            </div>
        );
    }
}

export default connect(
    state => (
        {"": state}
    ),
    dispatch => ({})
)(M_Doughnut)