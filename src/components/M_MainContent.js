import React, {Component} from 'react';
import {connect} from "react-redux";

class M_MainContent extends Component {

    render() {
        return (
            this.props.activeContent
        );
    };
}

export default connect(
    state => ({activeContent: state.mcs.activeContent}),
    dispatch => ({})
)(M_MainContent);