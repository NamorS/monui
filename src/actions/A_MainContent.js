export const clickEditPanel = () =>{
    return {type: 'EDIT_PANEL'}
};

export const onHideMenu = () =>{
  return {type: 'HIDE_MENU'}
};

export const toggleMenu = () =>{
    return {type: 'TOGGLE_MENU'}
};