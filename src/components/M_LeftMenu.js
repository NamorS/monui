import React, {Component} from 'react';
import {Menu, Input} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {menuItClick} from '../actions/A_Menu.js';

class M_LeftMenu extends Component {

    handleItemClick = (e, {name}) => {
        this.props.menuItClick(name);
    }

    render() {
        let activeItem = this.props.menuStore.activeItem;

        return (
            <Menu vertical fluid>
                <Menu.Item>
                    <Input icon='search' placeholder='Search by menu...'/>
                </Menu.Item>
                <Menu.Item name='dash' active={activeItem === 'dash'} onClick={this.handleItemClick}>
                    Dashboards
                </Menu.Item>
                <Menu.Item name='rep' active={activeItem === 'rep'} onClick={this.handleItemClick}>
                    Reports
                </Menu.Item>
                <Menu.Item name='notif' active={activeItem === 'notif'} onClick={this.handleItemClick}>
                    Notifications
                </Menu.Item>
                <Menu.Item name='up' active={activeItem === 'up'} onClick={this.handleItemClick}>
                    User Profile
                </Menu.Item>
            </Menu>
        )
    }
}

export default connect(state => ({menuStore: state.menu}), dispatch => ({
    menuItClick: (name) => {
        dispatch(menuItClick(name));
    }
}))(M_LeftMenu);
