import React, {Component} from 'react';
import {Menu, Icon} from 'semantic-ui-react';
import Name from './M_Name'
import {toggleLanguage} from "../actions/A_Menu";
import {connect} from "react-redux";
import {clickEditPanel, toggleMenu} from "../actions/A_MainContent";

class M_TopMenu extends Component {

    state = {
        curTime: new Date().toLocaleTimeString(),
        curDate: new Date().toDateString()
    };

    handleItemClick = (e, {name}) => {
        this.setState({activeItem: name});
        if (name === 'editPanel') {
            this.props.clickEditPanel();
        }
    };

    toggleLanguage = () => {
        this.props.toggleLanguage();
    };

    componentDidMount() {
        setInterval(() => {
            this.setState({
                curTime: new Date().toLocaleTimeString(),
                curDate: new Date().toDateString()
            })
        }, 1000)
    }

    render() {
        const {activeItem} = this.state;
        return (
            <Menu compact inverted size='small'>
                <Menu.Item
                    name='leftMenu'
                    active={activeItem === 'leftMenu'}
                    onClick={this.props.onToggle}
                >
                    <Icon className='blue large sidebar'/>
                </Menu.Item>

                <Menu.Item
                    name='editPanel'
                    active={activeItem === 'editPanel'}
                    onClick={this.handleItemClick}
                >
                    <Icon className='large blue window restore outline'/>
                </Menu.Item>

                <Menu.Item
                    position='left'
                >
                    {/*<Name text={{en: "Date", ru: "Дата"}}/> : */}{this.state.curDate}
                    {/*<Name text={{en: "Time", ru: "Время"}}/> : {this.state.curTime}*/}
                </Menu.Item>

                <Menu.Item link
                           name='profile'
                           active={activeItem === 'profile'}
                           onClick={this.handleItemClick}
                >
                    <Icon className='large blue user'/>
                    <Name text={{en: "Profile", ru: "Профиль"}}/>
                </Menu.Item>

                <Menu.Item link
                           name='settings'
                           active={activeItem === 'settings'}
                           onClick={this.handleItemClick}
                >
                    <Icon className='large blue setting'/>
                    <Name text={{en: "Settings", ru: "Настроки"}}/>
                </Menu.Item>

                <Menu.Item link
                           name='language'
                           active={activeItem === 'language'}
                           onClick={this.toggleLanguage}
                >
                    <Icon className='large blue globe'/>
                    <Name text={{en: "EN", ru: "RU"}}/>
                </Menu.Item>

                <Menu.Item link
                           name='logout'
                           active={activeItem === 'logout'}
                           onClick={this.handleItemClick}
                >
                    <Icon className='large blue power'/>
                    <Name text={{en: "Log out", ru: "Выход"}}/>
                </Menu.Item>
            </Menu>
        )
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        toggleLanguage: () => {
            dispatch(toggleLanguage());
        },
        clickEditPanel: () => {
            dispatch(clickEditPanel());
        },
    })
)(M_TopMenu);
