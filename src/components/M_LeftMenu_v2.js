import React, {Component} from 'react';
import {connect} from 'react-redux';
import {menuItClick} from '../actions/A_Menu.js';

class M_LeftMenu_v2 extends Component {

    handleItemClick = (e, {name}) => {
        this.props.menuItClick(name);
    }

    render() {
        let activeItem = this.props.menuStore.activeItem;

        return (
            <div className="left-side-menu">
                <nav className="main-menu">
                    <div>
                        <a className="logo" href="#">
                        </a>
                    </div>
                    <div className="settings">

                    </div>
                    <div className="scrollbar" id="style-1">
                        <ul>
                            <li>
                                <a href="#">
                                    <i className="fa fa-home fa-lg">

                                    </i>
                                    <span className="nav-text">Home</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i className="fa fa-user fa-lg">

                                    </i>
                                    <span className="nav-text">Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i className="fa fa-envelope-o fa-lg">

                                    </i>
                                    <span className="nav-text">Contact</span>
                                </a>
                            </li>
                            <li className="darkerlishadow">
                                <a href="#">
                                    <i className="fa fa-clock-o fa-lg">

                                    </i>
                                    <span className="nav-text">Link Sample</span>
                                </a>
                            </li>
                            <li className="darkerli">
                                <a href="#">
                                    <i className="fa fa-desktop fa-lg">

                                    </i>
                                    <span className="nav-text">Link Sample</span>
                                </a>
                            </li>
                            <li className="darkerli">
                                <a href="#">
                                    <i className="fa fa-align-left fa-lg">

                                    </i>
                                    <span className="nav-text">Link Sample</span>
                                </a>
                            </li>
                            <li className="darkerli">
                                <a href="#">
                                    <i className="fa fa-rocket fa-lg">

                                    </i>
                                    <span className="nav-text">Link Sample</span>
                                </a>
                            </li>
                            <li className="darkerlishadowdown">
                                <a href="#">
                                    <i className="fa fa-lightbulb-o fa-lg">

                                    </i>
                                    <span className="nav-text">Link Sample</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i className="fa fa-question-circle fa-lg">

                                    </i>
                                    <span className="nav-text">Help</span>
                                </a>
                            </li>
                        </ul>
                        <ul className="logout">
                            <li>
                                <a href="#">
                                    <i className="fa fa-sign-out fa-lg">

                                    </i>
                                    <span className="nav-text">LOG OUT</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
}

export default connect(state => ({menuStore: state.menu}), dispatch => ({
    menuItClick: (name) => {
        dispatch(menuItClick(name));
    }
}))(M_LeftMenu_v2);
