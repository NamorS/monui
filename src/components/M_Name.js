import React, {Component} from 'react';
import {connect} from "react-redux";

class M_Name extends Component {

    text = () => {
        if (this.props.ns.language === 'RU') {
            return this.props.text.ru;
        } else {
            return this.props.text.en;
        }
    };

    render() {
        const currentText = this.text();
        return (<div>{currentText}</div>);
    }
}

export default connect(
    state => ({ns: state.ns})
)(M_Name)