import React from "react";

export const asyncGetJSON = () => {

    return dispatch => {
        // TODO реализовать обработку всех возможных ошибок от бэкэнда
        fetch('http://192.168.122.10:3000/metrics?itemid=23295&stime=1518270644&etime=1518270944')
            .then(response => response.json())
            .then(json => {
                // TODO return state, который изменит состояние данных по графику
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const menuItClick = (name) => {
    return {type: 'ACTIVE_ITEM', menuItem: name};
};

export const toggleLanguage = () => {
  return {type: 'CHANGE_LANGUAGE'}
};
