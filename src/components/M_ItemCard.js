import React, {Component} from 'react';
import {Menu, Icon, Dropdown, Card, Button, Item, Segment, Image} from 'semantic-ui-react';
import M_LineChart from './M_LineChart';
import {connect} from "react-redux";
import {onClickClose} from "../actions/A_ItemCard";

class M_ItemCard extends Component {

    onClose = () => {
        this.props.onClickClose(this.props.id);
    };

    getTopCardMenu = () => {
        if (this.props.isEdit) {
            return (
                <Menu attached='top'>
                    <Dropdown item icon='wrench' simple>
                        <Dropdown.Menu>
                            <Dropdown.Header>Edit</Dropdown.Header>
                            <Dropdown.Item>
                                <Icon name='dropdown'/>
                                <span className='text'>New</span>
                                <Dropdown.Menu>
                                    <Dropdown.Item>Document</Dropdown.Item>
                                    <Dropdown.Item>Image</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown.Item>
                            <Dropdown.Item>Open</Dropdown.Item>
                            <Dropdown.Item>Save...</Dropdown.Item>
                            <Dropdown.Item>Edit Permissions</Dropdown.Item>
                            <Dropdown.Divider/>
                            <Dropdown.Header>Export</Dropdown.Header>
                            <Dropdown.Item>
                                <Icon name='dropdown'/>
                                <span className='text'>Share</span>
                                <Dropdown.Menu>
                                    <Dropdown.Item>E-mail</Dropdown.Item>
                                    <Dropdown.Item>Social</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    <Menu.Menu position='right'>
                        <Menu.Item link onClick={this.onClose}>
                            <Icon icon='x' name='x'/>
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
            )
        }

    };

    getButtonOpenChart = () => {
        if (!this.props.isEdit) {
            return (<Button attached='bottom' className='large teal' basic>Open Chart</Button>)
        }
    };

    getRightButtons = () => {
        return (
            <Button.Group size={'mini'} vertical>
                <Button inverted color='green' style={{height: 'auto'}} icon='angle double right'/>
                <Button inverted color='red' style={{height: 'auto'}} icon='angle double left'/>
            </Button.Group>
        )
    };

    getBottomButtons = () => {
        return (
            <Button.Group style={{width: '100%'}} size={'mini'}>
                <Button inverted color='red' icon='angle double up'/>
                <Button inverted color='green' icon='angle double down'/>
            </Button.Group>
        )
    };


    render() {
        const topMenu = this.getTopCardMenu();
        const rightButtons = this.getRightButtons();
        const bottomButtons = this.getBottomButtons();
        const isEdit = this.props.isEdit;
        return (
            <div style={{
                display: 'flex',
                justifyContent: 'space-around',
                minWidth: '33.3%',
                border: '2px solid lightgrey',
                borderRadius: '6px',
                backgroundColor: '#0b0a41',
                padding: '5px'
            }}>
                <div>
                    <Image src='https://www.investors.com/wp-content/uploads/2016/07/stock-bull-1-adobe.jpg'/>
                    {isEdit && bottomButtons}
                </div>
                {isEdit && rightButtons}
            </div>
        )
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        onClickClose: (id) => {
            dispatch(onClickClose(id));
        }
    })
)(M_ItemCard);