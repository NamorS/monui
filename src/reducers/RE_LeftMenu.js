const menuStore = {'activeItem': 'dash', 'visible' : false};

const RE_LeftMenu = (state = menuStore, action) => {
    if (action.type === 'ACTIVE_ITEM') {
        return {...state, 'activeItem': action.menuItem};
    }
    if (action.type === 'HIDE_MENU'){
        return {...state, 'visible': false}
    }
    if (action.type === 'TOGGLE_MENU'){
        return {...state, 'visible': !state.visible}
    }
    return state;
};

export default RE_LeftMenu;
