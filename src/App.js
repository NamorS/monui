import React, {Component} from 'react';
import {Menu, Segment, Sidebar} from "semantic-ui-react";
import {connect} from "react-redux";
import M_MainContent from './components/M_MainContent';
import Top_Menu from './components/M_TopMenu';
import {onHideMenu} from "./actions/A_MainContent";

class App extends Component {

    state = {
        visible: false
    };

    handleOnHide = () => {
        if(this.state.visible !== false) {
            this.setState({visible: false});
        }
    };

    toggleMenu = () => {
        this.setState({visible: !this.state.visible});
    };

    render() {
        const { visible } = this.state;
        return (
            <div style={{display: 'flex', height: '100%', flexDirection: 'column'}}>
                <Top_Menu onToggle={this.toggleMenu}/>
                <Sidebar.Pushable as={Segment} style={{marginTop: 0}}>
                    <Sidebar
                        onHide={this.handleOnHide}
                        as={Menu}
                        animation='overlay'
                        icon='labeled'
                        inverted
                        vertical
                        visible={visible}
                        width='thin'
                    >
                        <Menu.Item as='a'>
                            Dashboards
                        </Menu.Item>
                        <Menu.Item as='a'>
                            Reports
                        </Menu.Item>
                        <Menu.Item as='a'>
                            Notifications
                        </Menu.Item>
                        <Menu.Item as='a'>
                            TO DO
                        </Menu.Item>
                        <Menu.Item as='a'>
                            User Profile
                        </Menu.Item>
                    </Sidebar>

                    <Sidebar.Pusher dimmed={visible} style={{height: '100%', width: '100%'}}>
                        <Segment style={{height: '100%', marginTop: 0}}>
                            <M_MainContent/>
                        </Segment>
                    </Sidebar.Pusher>
                </Sidebar.Pushable>
            </div>
        )
    }
}

export default connect(
    state => ({menuStore: state.menu}),
    dispatch => ({
        onHideMenu: () => {
            dispatch(onHideMenu());
        }
    })
)(App);