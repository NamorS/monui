import {combineReducers} from 'redux';
import RE_LeftMenu from './RE_LeftMenu';
import RE_LineChart from './RE_LineChart';
import RE_Dashboard from "./RE_Dashboard";
import RE_MainContent from "./RE_MainContent";
import RE_Name from "./RE_Name";

const rootReducer = combineReducers({
    menu: RE_LeftMenu,
    lChart: RE_LineChart,
    dash: RE_Dashboard,
    mcs: RE_MainContent,
    ns: RE_Name
});

export default rootReducer;
