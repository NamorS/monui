const name = {language: 'RU'};

const RE_Name = (state = name, action) => {
    if (action.type === 'CHANGE_LANGUAGE') {
        if (state.language === 'RU') {
            return {language: 'EN'};
        } else {
            return {language: 'RU'};
        }
    }
    return state;
};

export default RE_Name;