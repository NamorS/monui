import M_Dashboard from "../components/M_Dashboard";
import React from "react";

const mainContentStore = {activeContent: <M_Dashboard/>};

const RE_MainContent = (state = mainContentStore, action) => {
    if (action.type === 'ACTIVE_ITEM') {
        let activeItem;
        switch (action.menuItem) {
            case 'dash':
                activeItem = <M_Dashboard/>;
                break;
            case 'rep':
                activeItem = <h1>Reports</h1>;
                break;
            case 'notif':
                activeItem = <h1>Notification</h1>;
                break;
            case 'up':
                activeItem = <h1>User Profile</h1>;
                break;
            default:
                activeItem = <M_Dashboard/>;
        }
        return {activeContent: activeItem}
    }
    return state;
};

export default RE_MainContent;