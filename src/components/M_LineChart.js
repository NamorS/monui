import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Line} from 'react-chartjs-2';


class M_LineChart extends Component {

    data = () => {
        // labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        return {
            labels: this.props.lChart.labels,
            datasets: [
                {
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: '#177',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: '#127',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 3,
                    pointHitRadius: 10,
                    data: this.props.lChart.data,
                    label: 'Здесь можно указать легенду',
                }
            ]
        }
    };

    // Опции для скрытия легенды
    options = () => ({legend: {display: false}});

    render() {
        return (
            <Line data={this.data} options={this.options()}/>
        );
    }
}

export default connect(
    state => (
        {lChart: state.lChart}
    ),
    dispatch => ({})
)(M_LineChart)
