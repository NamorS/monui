const dashboardStore = {
    items: {
        rows: [1,2,],
        cells: [1,2,3],
    },
    isEdit: false
};

const  RE_Dashboard = (state = dashboardStore, action) => {
    if(action.type === 'CLOSE_ITEMCARD'){
        return Object.assign({}, state, {
            cards: {
                ...state.cards,
                [action.id] : false
            }
        });
    }
    if(action.type === 'EDIT_PANEL'){
        return {...state, isEdit: !state.isEdit};
    }
    return state;
};

export default RE_Dashboard;