import React, {Component} from "react";
import {connect} from "react-redux";
import M_ItemCard from "./M_ItemCard";

class M_Dashboard extends Component {

    renderItems = (items, isEdit) => {
        const readyItems = items.rows.map((item, index) => {
            return (
                <div key={index} style={{display: 'flex'}}>
                    {items.cells.map((item, index) => {
                            return (
                                <M_ItemCard key={index} isEdit={isEdit}/>
                            );
                        }
                    )}
                </div>
            );
        });
        return (
            readyItems
        )
    };


    render() {
        const items = this.renderItems(this.props.dash.items, this.props.dash.isEdit);
        return (
            <div style={{display: 'flex', flexDirection: 'column'}}>
                {items}
            </div>
        )
    }
}

export default connect(
    state => ({dash: state.dash}),
    dispatch => ({})
)(M_Dashboard);