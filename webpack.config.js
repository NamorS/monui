const path = require('path');
const webpack = require('webpack');
const publicPath = '/dist/build/';
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = {
    mode: 'development',
    //Content
    entry: './index.js',

    devtool: 'inline-cheap-module-source-map',
    plugins: [
        //simplifies creation of HTML files to serve your webpack bundles. This is especially useful for webpack bundles that include a hash in the filename which changes every compilation. You can either let the plugin generate an HTML file for you, supply your own template using lodash templates or use your own loader.
        new HtmlWebpackPlugin({
            title: 'MoniTor',
            template: './src/index.html'
        }),
        //Auto replacement of page when i save some file, even css
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin("styles.css")
    ],

    output: {
        path: path.join(__dirname, publicPath),
        filename: '[name].bundle.js',
        publicPath: publicPath,
        sourceMapFilename: '[name].map',
    },

    devServer: {
        port: 3000,
        host: 'localhost',
        historyApiFallback: true,
        noInfo: false,
        stats: 'minimal',
        publicPath: publicPath,
        contentBase: path.join(__dirname, publicPath),
        hot: true
    },
    module: {
        rules: [
            {
                test: /\.js|\.jsx$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ['es2015', 'react', 'stage-2']
                }
            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                use: {
                    loader: 'url-loader',
                },
            },
            {
                test: /\.css$/,
                include: [/node_modules/, /src/],
                loader: ExtractTextPlugin.extract( 'css-loader', 'style-loader'),
            },
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};
